<?php

namespace App\Http\Controllers\V1;

use App\Classes\BigQuery\BigQueryParser;
use App\Classes\BigQuery\BigQuerySurvey;
use App\Classes\BigQuery\BigQueryBase;
use App\Classes\DialogFlow\Agent;
use App\Classes\MyValidator;
use App\Classes\Response;
use App\Http\Controllers\Controller;
use App\Models\Bots;
use Google\Cloud\BigQuery\Exception\JobException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BigqueryController extends Controller
{
    /**
     * @var \DateTime
     */
    private $date_utc;

    public function __construct()
    {
        $this->date_utc = new \DateTime("now", new \DateTimeZone("UTC"));
    }

    public function insertGenericSurvey(Request $request)
    {
        if($this->validateRequestBody($request->all())){
            Log::debug('Request');
            Log::debug(json_encode($request->all()));

            Log::debug('Cambiando formato');
            $request['response'] = addslashes(json_encode($request['response']));
            $request['survey_response'] = addslashes(json_encode($request['survey_response']));
            $request['tags'] = addslashes(json_encode($request['tags']));

            Log::debug('Iniciando servicio BigQuery');
            $bigquery_survey = new BigQuerySurvey('surveys');
            Log::debug('Insertando en BigQuery');
            $bigquery_survey->save_survey($request->all(), 'generic_survey');
            Log::debug('Terminó de insertar');
        }else{
            return \json_encode("error al validar entrada, checar los logs");
            Log::error('Respondiendo con error');
        }
    }

    private function validateRequestBody(array $request)
    {
        $validator = new MyValidator($request);
        Log::debug('Validando request');

        if (!$validator->validateChatbotId()) return false;
        if (!$validator->validateSessionId()) return false;
        if (!$validator->validateLang()) return false;
        if (!$validator->validateIntent()) return false;
        if (!$validator->validateSurveyResponse()) return false;
        if (!$validator->valid_RFC3339_EXTENDED_date($request['timestamp'])) return false;

        return true;
    }

}
