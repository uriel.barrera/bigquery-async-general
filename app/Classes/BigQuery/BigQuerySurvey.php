<?php

namespace App\Classes\BigQuery;

use Illuminate\Support\Facades\Log;

class BigQuerySurvey
{
    public $bq_results = array();
    private $session = '';
    private $bq_client;

    /**
     *@param string $database Name of database of bigquery
     */
    public function __construct($database)
    {
        $this->bq_client = new BigQueryBase($database);
        $this->bq_results = array();
        $this->session = '';
    }

    /**
     * Insert data of survey in BigQuery table 'survey'
     * 
     * @param array $data Data with key: value pair for insert in table 
     * @return void
     */
    public function save_survey($data, $table)
    {
        Log::debug('save_survey');
        $this->bq_client->insert_bigquery($table, $data);
    }


    private function get_timestamp()
    {
        $dt = new DateTime(date('Y-m-d H:i:s'));
        $fecha = $dt->format('Y-m-d H:i:s');
        return $fecha;
    }
}
