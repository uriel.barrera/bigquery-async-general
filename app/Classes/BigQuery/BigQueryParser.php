<?php

namespace App\Classes\BigQuery;

use Exception;
use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\ExponentialBackoff;
use Illuminate\Support\Facades\Log;

class BigQueryParser
{
    var $projectID;
    var $dataSet;
    var $table;
    var $legacySQL;
    var $serviceAccount;
    var $bqInterface;
    var $logger;

    public function __construct()
    {
        $this->projectID = config('app.project_id');
        $this->dataSet = '';
        $this->table = '';
        $this->legacySQL = '';
//        $this->serviceAccount = base_path(config('app.big_query_credentials'));
        $this->serviceAccount = credential('bigquery');
        $this->bqInterface = '';
        $this->connectBQ();
    }

    function connectBQ()
    {
        if ($this->projectID == null || $this->serviceAccount == null) {
            Log::error('Either the projectID or the serviceAccount is null');
            return null;
        }
        try {
            $this->bqInterface = new BigQueryClient([
                'projectId' => $this->projectID,
                'keyFilePath' => $this->serviceAccount
            ]);
        } catch (Exception $e) {
            Log::error('Exception Ocurred' . $e);
            return null;
        }
        return true;
    }

    function configQueryJob($query)
    {
        if ($this->bqInterface == null)
            return false;
        if ($this->legacySQL) {
            $options = array(
                'configuration' => array(
                    'query' => array(
                        'useLegacySql' => true
                    )
                )
            );
            return $this->bqInterface->query($query, $options);
        } else
            return $this->bqInterface->query($query);
    }

    function startQueryJob($jobConfig)
    {
        if ($jobConfig == null)
            return false;
        if ($this->bqInterface == null)
            return false;
        $job = $this->bqInterface->startQuery($jobConfig);
        $backoff = new ExponentialBackoff(10);
        $backoff->execute(function () use ($job) {
            $job->reload();
            if (!$job->isComplete())
                throw new Exception('BQ job has not yet completed', 500);
        });
        return $job->queryResults();
    }

    function setTable($tName)
    {
        if ($this->dataSet == '' || $this->dataSet == null)
            return false;
        $this->table = $this->bqInterface->dataset($this->dataSet)->table($tName);
        return true;
    }

    function loadFromGCS($gcsUri, $overWrite)
    {
        if ($this->table == '' || $this->table == null)
            return false;
        if ($gcsUri == '' || $gcsUri == null)
            return false;
        if ($overWrite)
            return $this->table->loadFromStorage($gcsUri)->autodetect(true)->skipLeadingRows(1)->writeDisposition('WRITE_TRUNCATE');
        else
            return $this->table->loadFromStorage($gcsUri)->autodetect(true)->skipLeadingRows(1);
    }

    function runTableJob($tableConfig)
    {
        if ($this->table == '' || $this->table == null)
            return false;

        if (!$tableConfig)
            return false;

        $job = $this->table->runJob($tableConfig);
        $backoff = new ExponentialBackoff(10);
        $backoff->execute(function () use ($job) {
            $job->reload();
            if (!$job->isComplete()) {
                throw new Exception('BQ job has not yet completed', 500);
                return false;
            }
        });

        if (isset($job->info()['status']['errorResult'])) {
            $error = $job->info()['status']['errorResult']['message'];
            printf('Error running job: %s' . PHP_EOL, $error);
            return false;
        } else {
            print('Data imported successfully' . PHP_EOL);
            return true;
        }

    }

    function setProjectID($pId)
    {
        $this->projectID = $pId;
    }

    function setServiceAccount($sA)
    {
        $this->serviceAccount = $sA;
    }

    function setLegacySQL($legacy)
    {
        $this->legacySQL = $legacy;
    }

    function setDataSet($dSet)
    {
        $this->dataSet = $dSet;
    }

    function getProjectID()
    {
        return $this->projectID;
    }

    function getServiceAccount()
    {
        return $this->serviceAccount;
    }

    function getDataSet()
    {
        return $this->dataSet;
    }

    function legacyOn()
    {
        return $this->legacySQL;
    }
}

?>
