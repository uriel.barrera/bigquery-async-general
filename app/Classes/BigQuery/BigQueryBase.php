<?php

namespace App\Classes\BigQuery;

use Google\Cloud\BigQuery\BigQueryClient;
use Illuminate\Support\Facades\Log;

class BigQueryBase
{
    private $bq_client;
    private $database;

    public function __construct(string $database)
    {
        Log::debug(credential('bigquery'));
        Log::debug(getenv('PROJECT_ID'));
        $this->bq_client = new BigQueryClient([
            'keyFilePath' => credential('bigquery'),
            'projectId' => getenv('PROJECT_ID')
        ]);
        $this->database = $database;
    }


    /**
     * Construct and run the SELECT query in bigquery
     * @param string $table Table name 
     * @param array $columns List of columns name to select. Default [] means SELECT *
     * @param array $where associative array with the key => value pair for where clause
     * @return array
     */
    public function select_info(string $table, array $where = [], $columns = [])
    {
        $results = [];
        $database = $this->database;
        $query_col = '';
        if (!empty($columns)) {
            $counter = 0;
            $lenght_col = count($columns);
            foreach ($columns as $col_name) {
                $query_col .= $col_name;
            }
            if ($counter < $lenght_col - 1) {
                $query_col .= ',';
            }
            $counter++;
        } else {
            $query_col = '*';
        }
        $query = <<<ENDSQL
            SELECT
            $query_col 
            FROM 
            `$database.$table`
            ENDSQL;
        if (!empty($where)) {
            $where_query = $this->get_query_where($where);
            $query .= " WHERE $where_query";
        }
        $rows = $this->execute_query($query);

        foreach ($rows as $row) {
            $results[] = $row;
        }
        return $results;
    }


    /**
     * Construct and run INSERT INTO in bigquery
     * @param string $table Table name
     * @param array $data Array key => value pair with the data to insert
     * @return bool 
     */
    public function insert_bigquery($table, $data)
    {
        Log::debug('insert_bigquery');
        Log::debug('tabla: ');
        Log::debug($table);

        Log::debug('datos: ');
        Log::debug($data);


        $keys = array_keys($data);
        $aux_keys_query = implode(',', $keys);
        $aux_values_query = $this->get_query_value($data, $table);
        $query = <<<ENDSQL
        INSERT `$this->database.$table` ($aux_keys_query)
        Values ($aux_values_query) 
        ENDSQL;
        $this->execute_query($query);

        return true;
    }

    private function get_query_value(array $data, string $table)
    {
        $aux_values_query = '';
        $counter = 0;
        $lenght_data = count($data);
        foreach ($data as $key => $value) {
            if ($value == null) {
                continue;
            }
            if (is_array($value)) {
                if (!empty($value)) {
                    if (is_associative_array($value)) {
                        $aux_values_query .= "(SELECT AS STRUCT ANY_VALUE($key).* REPLACE ";
                        $aux_values_query .= '(' . $this->get_query_struct($value, $table) . ')';
                        $aux_values_query .= " FROM `tickets.$table` WHERE 1=0)";
                    } else {
                        $aux_values_query .= $this->get_query_array_no_associative($value);
                    }
                } else {
                    continue;
                }
            } else if (is_string($value)) {
                $aux_values_query .= "'$value'";
            } else if (is_bool($value))
            {
                if ($value == true)
                {
                    $aux_values_query .= "True";
                } else 
                {
                    $aux_value_query .= "False";
                }
            } else {
                $aux_values_query .= "$value";
            }
            if ($counter < $lenght_data - 1) {
                $aux_values_query .= ',';
            }
            $counter++;
        }

        return $aux_values_query;
    }


    private function get_query_struct(array $data, string $table)
    {
        $aux_query = '';
        $counter = 0;
        $lenght_data = count($data);
        foreach ($data as $key => $value) {
            if ($value == null) {
                continue;
            }
            if (is_array($value)) {
                if (!empty($value)) {
                    if (is_associative_array($value)) {
                        $aux_query .= "(SELECT AS STRUCT ANY_VALUE($key).* REPLACE ";
                        $aux_query .= '(' . $this->get_query_struct($value, $table) . ')';
                        $aux_query .= " FROM `tickets.$table` WHERE 1=0)";
                    } else {
                        $aux_query .= $this->get_query_associative($value);
                    }
                } else {
                    continue;
                }
            } else if (is_string($value)) {
                $aux_query .= "'$value' AS $key";
            } else {
                $aux_query .= "'$value' AS $key";
            }
            if ($counter < $lenght_data - 1) {
                $aux_query .= ',';
            }
            $counter++;
        }

        return $aux_query;
    }


    private function get_query_array_no_associative(array $data)
    {
        $aux_query = '[';
        $counter = 0;
        $lenght_data = count($data);
        foreach ($data as $val) {
            if (is_string($val)) {
                $aux_query .= "'$val'";
            } else {
                $aux_query .= "$val";
            }
            if ($counter < $lenght_data - 1) {
                $aux_query .= ',';
            }
            $counter++;
        }
        $aux_query .= ']';

        return $aux_query;
    }


    private function get_query_where(array $data)
    {
        $aux_query_where = '';
        $counter = 0;
        $lenght_data = count($data);
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $aux_query_where .= "$key = '$value'";
            } else {
                $aux_query_where .= "$key = $value";
            }
            if ($counter < $lenght_data - 1) {
                $aux_query_where .= ',';
            }
            $counter++;
        }

        return $aux_query_where;
    }


    private function get_query_set(array $data, string $table, array $insert)
    {
        $aux_query = '';
        $counter = 0;
        $lenght_data = count($data);
        foreach ($data as $key => $value) {
            if ($value == null) {
                continue;
            }
            if (is_array($value)) {
                if (!empty($value)) {
                    if (is_associative_array($value)) {
                        $aux_query = $this->get_query_array_associative($key, $value);
                    } else {
                        $no_ass_query = $this->get_query_array_no_associative($value);
                        $aux_query .= "$key = $no_ass_query ";
                    }
                } else {
                    continue;
                }
            } else if (is_string($value)) {
                $aux_query .= "$key = '$value'";
            } else {
                $aux_query .= "$key = $value";
            }
            if ($counter < $lenght_data - 1) {
                $aux_query .= ',';
            }
            $counter++;
        }

        return $aux_query;
    }


    private function get_query_array_associative($key, $data)
    {
        $value = array_filter($data);
        $aux_query = '';
        $counter = 0;
        $lenght_data = count($value);
        foreach ($value as $k_val => $v_val) {
            if ($v_val == null) {
                continue;
            }
            if (is_string($v_val)) {
                $aux_query .= "$key.$k_val = '$v_val'";
            } else {
                $aux_query .= "$key.$k_val = '$v_val'";
            }
            if ($counter < $lenght_data - 1) {
                $aux_query .= ',';
            }
            $counter++;
        }

        return $aux_query;
    }

    /**
     * Returns the result for a query.
     * 
     * @param string $query 
     * @return $rows
     */
    private function execute_query(string $query)
    {
        Log::debug('execute_query, query: ');
        Log::debug($query);
        
        try {
            $rows = [];
            $queryJobConfig = $this->bq_client->query($query);
            $queryResults = $this->bq_client->runQuery($queryJobConfig);
            if ($queryResults->isComplete()) {
                $rows = $queryResults->rows();
                return $rows;
            } else {
                custom_log('error', "The query $query failed to complete");
                return [];
            }
        } catch (Exception $e) {
            custom_log('error', 'Error to execute query: ' . $query . "\nCaught exception: ",  $e->getMessage(), "\n");
            return [];
        }
    }
}
