<?php
namespace App\Classes\Validations;

use Illuminate\Support\Facades\Log;

class LoggerValidator
{
    var $logger;
    var $requestBody;

    public function __construct(array $requestBody)
    {
        $this->requestBody = $requestBody;
    }

    public function validateSessionId()
    {
        if (!isset($this->requestBody['session_id']) || !$this->requestBody['session_id']) {
            Log::error('session_id not found');
            return false;
        }
        return true;
    }

    public function validateProjectId()
    {
        if (!isset($this->requestBody['project_id']) || !$this->requestBody['project_id']) {
            Log::error('project_id not found');
            return false;
        }
        return true;
    }

    public function validateLang()
    {
        if (!isset($this->requestBody['lang']) || !$this->requestBody['lang']) {
            Log::error('lang not found');
            return false;
        }
        return true;
    }

    public function validateIntent()
    {
        if (!isset($this->requestBody['intent']) || !$this->requestBody['intent']) {
            Log::error('intent not found');
            return false;
        }
        return true;
    }

    public function validateChatbotId()
    {
        if (!isset($this->requestBody['chatbot_id']) || !$this->requestBody['chatbot_id']) {
            Log::error('chatbot_id not found');
            return false;
        }
        return true;
    }

    public function validateQuery()
    {
        if (!isset($this->requestBody['query'])) {
            Log::error('query not found');
            return false;
        }
        return true;
    }

    public function validateResponse()
    {
        if (!isset($this->requestBody['response']) || !$this->requestBody['response']) {
            Log::error('response not found');
            return false;
        }
        return true;
    }

    public function validateStatusAgent()
    {
        if (!isset($this->requestBody['status_agent']) || !$this->requestBody['status_agent']) {
            Log::error('status_agent not found');
            return false;
        }
        return true;
    }

    public function validateStatusIntent()
    {
        if (!isset($this->requestBody['status_intent']) || !$this->requestBody['status_intent']) {
            Log::error('status_intent not found');
            return false;
        }
        return true;
    }

}
