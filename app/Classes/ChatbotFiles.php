<?php

namespace App\Classes;

use App\Classes\CloudStorage\CloudStorageInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;

class ChatbotFiles
{
    var $chatbot = null;
    var $cloudstorage = null;
    var $path = null;
    private $folder_assets;

    public function __construct($chatbot = null)
    {
        $this->folder_assets = config('app.folder_assets');
        if ($chatbot) {
            $this->chatbot = $chatbot;
            $this->setProjectURI();
        }
        $this->cloudstorage = new CloudStorageInterface();
    }

    public function setChatbot($chatbot)
    {
        $this->chatbot = $chatbot;
        $this->setProjectURI();
    }

    private function setProjectURI()
    {
        if ($this->chatbot) {
            $this->path = $this->folder_assets . '/' . $this->chatbot->owner->id . '_' . $this->chatbot->owner->name . '/chatbot_files/' . $this->chatbot->id;
        }
    }

    private function getProjectURI()
    {
        return $this->path;
    }

    public function getLogo()
    {
        $path = $this->getProjectURI();
        $nextDay = time() + (1 * 24 * 60 * 60);

        if ($path) {
            $object = $this->cloudstorage->getFile($path . '/interface_assets/logo.png');
            if ($object->exists()) {
                return $object->signedUrl($nextDay);
            }
        }
        return asset('img/logo.png');
    }

    public function getIconChatbot()
    {
        $path = $this->getProjectURI();
        $nextDay = time() + (1 * 24 * 60 * 60);

        if ($path) {
            $object = $this->cloudstorage->getFile($path . '/interface_assets/icon_chatbot.png');
            if ($object->exists()) {
                return $object->signedUrl($nextDay);
            }
        }
        return asset('img/icon_chatbot.png');
    }

    public function getIconUser()
    {
        $path = $this->getProjectURI();
        $nextDay = time() + (1 * 24 * 60 * 60);

        if ($path) {
            $object = $this->cloudstorage->getFile($path . '/interface_assets/icon_user.png');
            if ($object->exists()) {
                return $object->signedUrl($nextDay);
            }
        }
        return asset('img/icon_user.png');
    }

    public function getIconChat()
    {
        $path = $this->getProjectURI();
        $nextDay = time() + (1 * 24 * 60 * 60);

        if ($path) {
            $object = $this->cloudstorage->getFile($path . '/interface_assets/icon_chat.png');
            if ($object->exists()) {
                return $object->signedUrl($nextDay);
            }
        }
        return asset('img/icon_chat.svg');
    }

    public function saveLogo(UploadedFile &$file, Response &$response)
    {
        if ($file->getMimeType() != 'image/png') {
            $response->response('El archivo debe ser de tipo image/png', 400)->send();
        }

        if ($file->getSize() > 2097152) {
            $response->response('El archivo no debe superar las ', 400)->send();
        }

        $image = getimagesize($file);
        $width = $image[0];
        $height = $image[1];
        if ($width > 180 || $height > 180) {
            $response->response('Las dimenciones de la imagen deben ser maximo 180x180 px', 400)->send();
        }

        $path = $this->getProjectURI();

        if ($this->cloudstorage->saveFile($path . '/interface_assets/logo.png', $file->path())) {
            return true;
        } else {
            return false;
        }
    }

    public function saveIconChatbot(UploadedFile &$file, Response &$response)
    {
        if ($file->getMimeType() != 'image/png') {
            $response->response('El archivo debe ser de tipo image/png', 400)->send();
        }

        if ($file->getSize() > 2097152) {
            $response->response('El archivo no debe superar las ', 400)->send();
        }

        $image = getimagesize($file);
        $width = $image[0];
        $height = $image[1];
        if ($width > 110 || $height > 110) {
            $response->response('Las dimenciones de la imagen deben ser maximo 110x110 px', 400)->send();
        }

        $path = $this->getProjectURI();

        if ($this->cloudstorage->saveFile($path . '/interface_assets/icon_chatbot.png', $file->path())) {
            return true;
        } else {
            return false;
        }
    }

    public function saveIconUser(UploadedFile &$file, Response &$response)
    {
        if ($file->getMimeType() != 'image/png') {
            $response->response('El archivo debe ser de tipo image/png', 400)->send();
        }

        if ($file->getSize() > 2097152) {
            $response->response('El archivo no debe superar las ', 400)->send();
        }

        $image = getimagesize($file);
        $width = $image[0];
        $height = $image[1];
        if ($width > 110 || $height > 110) {
            $response->response('Las dimenciones de la imagen deben ser maximo 110x110 px', 400)->send();
        }

        $path = $this->getProjectURI();

        if ($this->cloudstorage->saveFile($path . '/interface_assets/icon_user.png', $file->path())) {
            return true;
        } else {
            return false;
        }
    }

    public function saveIconChat(UploadedFile &$file, Response $response)
    {
        if ($file->getMimeType() != 'image/png') {
            $response->response('El archivo debe ser de tipo image/png', 400)->send();
        }

        if ($file->getSize() > 2097152) {
            $response->response('El archivo no debe superar las ', 400)->send();
        }

        $image = getimagesize($file);
        $width = $image[0];
        $height = $image[1];
        if ($width > 110 || $height > 110) {
            $response->response('Las dimenciones de la imagen deben ser maximo 110x110 px', 400)->send();
        }

        $path = $this->getProjectURI();

        if ($this->cloudstorage->saveFile($path . '/interface_assets/icon_chat.png', $file->path())) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteImageAssets($file_name)
    {
        $path = $this->getProjectURI();
        $this->cloudstorage->deleteFile($path . '/interface_assets/' . $file_name . '.png');
    }

    public function getDialogFlowCredentials()
    {
        $path = $this->getProjectURI();
        $object = $this->cloudstorage->getFile($path . '/service_accounts/' . $this->chatbot->project_id . '-dialogflow.json');
        if ($object->exists()) {
            $gcsURI = $object->gcsUri();
            $file = file_get_contents($gcsURI);
            $jsonContents = json_decode($file, true);
            if ($jsonContents == null || $jsonContents == '') {
                Log::warning(' No valid JSON Format on file' . $object->name() . ' at ' . $gcsURI);
                Log::debug(sprintf($file));
                return null;
            }
            return $jsonContents;
        } else {
            return null;
        }
    }

    public function getFirebaseCredentials()
    {
        $path = $this->getProjectURI();
        $object = $this->cloudstorage->getFile($path . '/service_accounts/' . $this->chatbot->project_id . '-firebase.json');
        if ($object->exists()) {
            $gcsURI = $object->gcsUri();
            $jsonContents = file_get_contents($gcsURI);
            if ($jsonContents == null || $jsonContents == '') {
                Log::warning(' No valid JSON Format on file' . $object->name() . ' at ' . $gcsURI);
                Log::debug(sprintf($jsonContents));
                return null;
            }
            return $jsonContents;
        } else {
            return null;
        }
    }
}
