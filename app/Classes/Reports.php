<?php


namespace App\Classes;

use App\Models\Bots;
use App\Models\Reports as ModelReports;
use Exception;
use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\ExponentialBackoff;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Reports
{
    function __construct()
    {
    }

    private function getWhereDateStart($start_date)
    {
        if ($start_date) {
            $_where_start_date = ' AND DATE(timestamp, "America/Mexico_City") >= "' . $start_date . '" ';
        } else {
            $_where_start_date = ' AND DATE(timestamp, "America/Mexico_City") >= "' . date('Y-m-d') . '" ';
        }
        return $_where_start_date;
    }

    private function getWhereDateEnd($end_date)
    {
        $_where_end_date = '';
        if ($end_date) {
            $_where_end_date = ' AND DATE(timestamp, "America/Mexico_City") <= "' . $end_date . '" ';
        }
        return $_where_end_date;
    }

    private function getWhereDate($date)
    {
        $_where_date = '';
        if ($date) {
            $_where_date = ' AND DATE(timestamp, "America/Mexico_City") = "' . $date . '" ';
        }
        return $_where_date;
    }

    private function getWhereStatus($status)
    {
        $_where_status = '';
        if ($status != 'all') {
            $_where_status = ' AND status_intent = "' . $status . '" ';
        }
        return $_where_status;
    }

    private function getWhereChatbot($chatbot_id)
    {
        $user_bots = new UserBots(Auth::id());
        $bots = $user_bots->getAllBots();

        if (!$chatbot_id || $chatbot_id == 0) {
            if ($bots) {
                $_where_chatbot_id = ' (';
                $i = 0;
                foreach ($bots as $bot) {
                    if ($i != 0) {
                        $_where_chatbot_id .= ' OR ';
                    }
                    $_where_chatbot_id .= ' chatbot_id = ' . $bot['id'];
                    $i++;
                }

                $_where_chatbot_id .= ')';
                return $_where_chatbot_id;
            }
        } else {
            if (isset($bots[$chatbot_id])) {
                $_where_chatbot_id = ' chatbot_id = ' . $chatbot_id;
                return $_where_chatbot_id;
            }
        }
        return null;
    }

    private function getWhereLang($lang)
    {
        $_where_lang = '';
        if ($lang != 0) {
            $lang = (new \App\Models\Lang)->getLangById($lang)->abbreviation;
            $_where_lang = ' AND lang = "' . $lang . '" ';
        }
        return $_where_lang;
    }

    private function getWhereOrigin($origin)
    {
        $_where_origin = '';
        if ($origin != 0) {
            $origin = (new \App\Models\Origin())->getOriginById($origin)->origin;
            $_where_origin = ' AND origin = "' . $origin . '" ';
        }
        return $_where_origin;
    }

    public function exportMassData(ModelReports $report, $fields, $columns_name)
    {
        $query = $this->generateQueryBulkDownload($report, $fields);

        try {
            $job_query = $this->consultBulkDownload($report, $query);
        } catch (Exception $e) {
            Log::error('Error generating export job: '.json_encode($e));
            $report->status = 5;
            $report->save();
            return response()->json('Query fail', 503);
        }


        if ($job_query->info()['status']['state'] != 'DONE') {
            log::error('Query fail: ' . json_encode($job_query->info()) . ' Change report to status 5');
            $report->status = 5;
            $report->save();

            return response()->json('Query fail', 503);
        } else {
            Log::info('Begin export data from big query,nos change status report to status 3');
            $report->status = 3;
            $report->save();
        }

        try {
            $job_export = $this->exportBulkDownload($report, $job_query);
        } catch (Exception $e) {
            Log::error('Error generating export job: '.json_encode($e));
            $report->status = 5;
            $report->save();
            return response()->json('Query fail', 503);
        }

        if ($job_export->info()['status']['state'] != 'DONE') {
            log::error('Export fail: ' . json_encode($job->info()) . ' Change report to status 5');
            $report->status = 5;
            $report->save();

            return response()->json('Query fail', 503);
        } else {
            Log::info('Getting num of files generated');
            Log::info('Job export success, change status report to status 4');
            $num_of_files = $job_export->info()['statistics']['extract']['destinationUriFileCounts'][0];
            $report->status = 4;
            $report->number_of_files_generated = $num_of_files;
            $report->save();
        }

        return response()->json('BULK DOWNLOAD COMPLETE', 200);
    }

    private function generateQueryBulkDownload(ModelReports $report, $fields) {
        $payload = json_decode($report->payload);

        $_where_chatbot_id = ' chatbot_id = ' . $report->bot_id;

        if ($_where_chatbot_id === false) return false;

        $_where_start_date = $this->getWhereDateStart($payload->start_date);

        $_where_end_date = $this->getWhereDateEnd($payload->end_date);

        $_where_origin = '';
        $_where_lang = '';

        if ($payload->origin === 'true' || $payload->origin === true) {
            $_where_origin = $this->getWhereOrigin($payload->filter_origin);
        }

        if ($payload->lang === 'true' || $payload->lang === true) {
            $_where_lang = $this->getWhereLang($payload->filter_lang);
        }

        $_order = 'DESC';
        if ($payload->order === 'ASC') {
            $_order = $payload->order;
        }

        $query = "SELECT " . implode(',', $fields) . ",DATETIME(timestamp, 'America/Mexico_City') as date FROM `" . config('app.project_id') . "." . config('bq_logs.dataset') . "." . config('bq_logs.table_chatbots') . "` WHERE $_where_chatbot_id $_where_start_date $_where_end_date $_where_origin $_where_lang ORDER BY date " . $_order;
        return $query;
    }

    /**
     * @param ModelReports $report
     * @param string $query
     * @return \Google\Cloud\BigQuery\Job
     * @throws Exception
     */
    private function consultBulkDownload(ModelReports $report, string $query): \Google\Cloud\BigQuery\Job
    {
        $bigQuery = new BigQueryClient([
            'projectId' => config('app.project_id'),
            'keyFilePath' => credential('bigquery_and_cloud_storage')
        ]);

        $jobConfig = $bigQuery->query($query);
        $job = $bigQuery->startQuery($jobConfig);
        if ($job) {
            Log::info('Getting data from big query, change status report to status 2');
            $report->status = 2;
            $report->save();
        }

        $backoff = new ExponentialBackoff(20);
        $backoff->execute(function () use ($job) {
            Log::info('Waiting for query job to complete');
            $job->reload();
            if (!$job->isComplete()) {
                Log::info('Job has not yet completed');
                throw new Exception('Job has not yet completed', 500);
            }
        });

        return $job;
    }

    /**
     * @param \Google\Cloud\BigQuery\Job $job_query
     * @param ModelReports $report
     * @return \Google\Cloud\BigQuery\Job
     * @throws Exception
     */
    private function exportBulkDownload(ModelReports $report, \Google\Cloud\BigQuery\Job $job_query): \Google\Cloud\BigQuery\Job
    {
        $bigQuery = new BigQueryClient([
            'projectId' => config('app.project_id'),
            'keyFilePath' => credential('bigquery_and_cloud_storage')
        ]);

        $datasetId = $job_query->info()['configuration']['query']['destinationTable']['datasetId'];
        $tableId = $job_query->info()['configuration']['query']['destinationTable']['tableId'];

        $chatbot = Bots::whereId($report->bot_id)->first();
        $account = $chatbot->owner();

        $cloud_storage_taget = config('app.bucked_name') . '/' . config('app.folder_assets') . '/' . $account->id . '_' . $account->unique_name . '/chatbot_files/' . $chatbot->id . '/reports/' . $report->id;

        $dataset = $bigQuery->dataset($datasetId);
        $table = $dataset->table($tableId);
        $destinationUri = "gs://{$cloud_storage_taget}/report-*.csv";
        $extractConfig = $table->extract($destinationUri);
        $job_export = $table->runJob($extractConfig);

        $backoff = new ExponentialBackoff(20);
        $backoff->execute(function () use ($job_export) {
            Log::info('Waiting for export job to complete');
            $job_export->reload();
            if (!$job_export->isComplete()) {
                Log::info('Job has not yet completed');
                throw new Exception('Job has not yet completed', 500);
            }
        });

        return $job_export;
    }
}
