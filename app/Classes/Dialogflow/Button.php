<?php

namespace App\Classes\DialogFlow;

class Button
{
    use RichResponseBase;
    var $text;
    var $postback;

    public function __construct($txt = '', $pback = null)
    {
        $this->text = $txt;
        if ($pback == null)
            $this->postback = $txt;
        else
            $this->postback = $pback;
    }

    public function fromJSON($json = false)
    {
        if ($json) {
            $parsJSON = json_decode($json, true);
            if ($parsJSON == null || $parsJSON == '')
                return false;
            foreach ($parsJSON as $key => $value) {
                if (isset($this->{$key}))
                    $this->{$key} = $value;
            }
            return true;
        }
        return false;
    }
}

?>
