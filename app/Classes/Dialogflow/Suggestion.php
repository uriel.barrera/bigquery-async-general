<?php

namespace App\Classes\DialogFlow;

class Suggestion
{
    use RichResponseBase;
    var $text;

    public function __construct($txt = null)
    {
        $this->text = $txt;
    }

    public function setTxt($txt)
    {
        $this->text = $txt;
    }

    public function getTxt($txt)
    {
        return $this->text;
    }
}

?>
