<?php

namespace App\Classes\DialogFlow;

class Response
{
    var $fulfillmentText;
    var $fulfillmentMessages;
    var $quickReplies;
    var $suggestions;
    var $origin;
    var $platform;

    public function __construct($source = "")
    {
        $this->fulfillmentText = '';
        $this->fulfillmentMessages = [];
        $this->quickReplies = array();
        $this->origin = $source;
    }

    public function addFullfilmentMessage($element)
    {
        if ($element != null) {
            if (is_array($this->fulfillmentMessages)) {
                $this->fulfillmentMessages[] = $this->parseMsgToPlatform($element);
            } else {
                $auxElement = $this->fulfillmentMessages;
                $this->fulfillmentMessages = ($auxElement == null ? array($this->parseMsgToPlatform(element)) : array($auxElement, $this->parseMsgToPlatform($element)));
            }
        }
    }

    public function addQuickReplies($element)
    {
        $type = ($this->origin == 'google' ? 'suggestions' : 'quickReplies');
        $platform = ($this->origin == 'google' ? 'ACTIONS_ON_GOOGLE' : strtoupper($this->origin));
        if ($element != null) {
            if (is_array($this->quickReplies)) {
                $this->quickReplies[$type][] = $this->parseMsgToPlatform($element);
            } else {
                $auxElement = $this->quickReplies;
                $this->quickReplies = array($type => ($auxElement == null ? array($this->parseMsgToPlatform($element)) : array($auxElement, $this->parseMsgToPlatform($element))));
            }
        }
    }

    public function setfulfillmentText($txt)
    {
        $this->fulfillmentText = ($txt != null ? $txt : '');
    }

    private function parseMsgToPlatform($message = "")
    {
        $newMessage = array();
        if ($message == "")
            return false;
        if (is_object($message)) {

            $class = class_basename($message);
        }
        else {
            $class = (is_array($message) ? 'array' : '');
        }

        if ($this->origin == null || $this->origin == "") {
            if ($class == 'Text') {
                if (is_object($message))
                    $contenido = $message->{strtolower($class)};
                else
                    $contenido = $message[strtolower($class)];
                $newMessage['text'] = array('text' => $contenido);
            } elseif ($class == 'Suggestion') {
                return $message->text;
            } elseif ($class == 'Card')
                $newMessage['card'] = $message;
            else
                return $message;
            return $newMessage;
        }
        if ($this->origin == "google") {
            $platform = 'ACTIONS_ON_GOOGLE';
            $newMessage['platform'] = $platform;
            if ($class == 'Card') {
                $newMessage['basicCard'] = array(
                    'title' => $message->title,
                    'formattedText' => $message->subtitle
                );
                if ($message->imageUri != null)
                    $newMessage['image'] = array(
                        'imageUri' => $message->imageUri,
                        'accessibilityText' => $message->subtitle
                    );
                if ($message->buttons != null)
                    $newMessage['basicCard']['buttons'] = array();
                if (!empty($message->buttons)) {
                    foreach ($message->buttons as $button) {
                        $postBack = (is_object($button) ? $button->postback : $button['postback']);
                        $texto = (is_object($button) ? $button->text : $button['text']);
                        $newButton = array(
                            'title' => $texto,
                            'openUriAction' => array(
                                'uri' => $postBack
                            )
                        );
                        $newMessage['basicCard']['buttons'][] = $newButton;
                    }
                }
            }
            if ($class == 'Suggestion') {
                $newMessage = array(
                    'title' => $message->text
                );
            }
            if ($class == 'Text') {
                $newMessage['simpleResponses'] = array(
                    'simpleResponses' => [array(
                        'textToSpeech' => $message->text[0],
                        'displayText' => $message->text[0]
                    )
                    ]
                );
            }
            if ($class == 'array')
                return $message;
        } else {
            $platform = strtoupper($this->origin);
            $newMessage['platform'] = $platform;
            if ($class == 'Text') {
                if (is_object($message))
                    $contenido = $message->{strtolower($class)};
                else
                    $contenido = $message[strtolower($class)];
                $newMessage['text'] = array('text' => $contenido);
            } elseif ($class == 'Card') {
                $newMessage['card'] = array(
                    'title' => $message->title,
                    'subtitle' => $message->subtitle,
                    'imageUri' => $message->imageUri,
                    'buttons' => $message->buttons
                );
            } elseif ($class == 'Suggestion')
                $newMessage['quickReplies'] = $message->text;
        }
        return $newMessage;
    }
}

?>
