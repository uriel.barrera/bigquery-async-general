<?php

namespace App\Classes\DialogFlow;

define('MESSAGE_CLASSES', array('Text', 'Card'));
define('REPLIES_CLASSES', array('Suggestion'));

class Agent
{
    var $response;
    var $origin;

    public function __construct(String $origin = '')
    {
        $this->response = new Response($origin);
        $this->origin = $origin;
    }
    #----------------------------------------------------
    # Add new element to the reponse
    #----------------------------------------------------
    public function add($element = null)
    {
        if ($element != null) {
            if (is_object($element)) {
                $clase = get_class($element);
                if (in_array($clase, MESSAGE_CLASSES)) {
                    $this->response->addFullfilmentMessage($element);
                }
                if (in_array($clase, REPLIES_CLASSES))
                    $this->response->addQuickReplies($element);
            } else {
                $temp = new Text($element);
                $this->response->addFullfilmentMessage($temp);
            }
        }
    }
    #------------------------------------------------------
    # Add error message
    #------------------------------------------------------
    public function addError($error = 'Error en Webhook')
    {
        $this->response->setfulfillmentText($error);
        $this->response->fulfillmentMessages = array();
    }
    #-------------------------------
    # Print our response messages
    #-------------------------------
    public function printAnswer()
    {
        $type = ($this->origin == 'google' ? 'suggestions' : 'quickReplies');
        $platform = ($this->origin == 'google' ? 'ACTIONS_ON_GOOGLE' : strtoupper($this->origin));
        if (!empty($this->response->quickReplies)) {
            $this->response->addFullfilmentMessage(array($type => $this->response->quickReplies, 'platform' => $platform));
        }
        $response = array();
        $response['fulfillmentMessages'] = $this->response->fulfillmentMessages;
        $response['outputContexts'] = [];
        $this->response = $response;
        $message = json_encode($response);
        echo($message);
    }
}

?>
