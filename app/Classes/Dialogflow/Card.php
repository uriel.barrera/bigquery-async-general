<?php

namespace App\Classes\DialogFlow;

class Card
{
    use RichResponseBase;
    var $title;
    var $subtitle;
    var $imageUri;
    var $buttons;
    var $text;

    public function __construct($titulo = 'title', $subtitulo = 'subtitle', $txt = '', $img = '', $btns = array())
    {
        $this->title = $titulo;
        $this->subtitle = $subtitulo;
        $this->text = $txt;
        $this->imageUri = $img;
        $this->buttons = $btns;
    }

    public function addButton($btn = null)
    {
        if ($btn != null) {
            if (is_array($this->buttons)) {
                $this->buttons[] = $btn;
            } else {
                $auxBtns = $this->buttons;
                $this->buttons = array(
                    $auxBtns,
                    $btn
                );
            }
        }
    }
}

?>
