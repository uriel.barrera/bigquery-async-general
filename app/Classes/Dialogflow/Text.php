<?php

namespace App\Classes\DialogFlow;
#----------------------------------
# Class Text for DialogFlow
#----------------------------------

class Text
{
    use RichResponseBase;
    var $text;

    public function __construct($txt = null)
    {
        $this->text = array($txt);
    }

    public function setTxt($txt)
    {
        $this->text = array($txt);
    }

    public function getTxt($txt)
    {
        return $this->text;
    }
}

?>
