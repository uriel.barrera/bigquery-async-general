<?php

namespace App\Classes\DialogFlow;

trait RichResponseBase
{
    public function fromJSON($json = false)
    {
        if ($json) {
            $parsJSON = json_decode($json, true);
            if ($parsJSON == null || $parsJSON == '')
                return false;
            foreach ($parsJSON as $key => $value) {
                if (isset($this->{$key}))
                    $this->{$key} = $value;
            }
            return true;
        }
        return false;
    }

    public function replaceParameters($parameters = null, $messageObj = '')
    {
        if (is_array($parameters)) {
            foreach ($parameters as $pKey => $pValue) {
                $parameterReplace = '__' . $pKey . '__';
                $messageObj = str_replace($parameterReplace, $pValue, $messageObj);

            }
        }
        $regex = '/__\w*__/';
        $messageObj = preg_replace($regex, '', $messageObj);
        return $messageObj;
    }
}

?>
