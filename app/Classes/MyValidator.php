<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;

class MyValidator
{
    var $logger;
    var $requestBody;
    
    public function __construct(array $requestBody)
    {
        $this->requestBody = $requestBody;
    }
    
    public function validateChatbotId()
    {
        if (!isset($this->requestBody['chatbot_id']) || !$this->requestBody['chatbot_id']) {
            Log::error('chatbot_id not found');
            return false;
        }

        if (!intval($this->requestBody['chatbot_id'])) {
            Log::error('chatbot_id is invalid');
            return false;
        }

        return true;
    }
    
    public function validateSessionId()
    {
        if (!isset($this->requestBody['session_id']) || !$this->requestBody['session_id']) {
            Log::error('session_id not found');
            return false;
        }
        return true;
    }

    public function validateLang()
    {
        if (!isset($this->requestBody['lang']) || !$this->requestBody['lang']) {
            Log::error('lang not found');
            return false;
        }

        preg_match('/es|en|fr/i', $this->requestBody['lang'], $matches);
        if ($matches) {
            $this->requestBody['lang'] = $matches[0];
        } else {
            Log::error('lang not valid');
            return false;
        }

        return true;
    }

    public function validateIntent()
    {
        if (!isset($this->requestBody['intent']) || !$this->requestBody['intent']) {
            Log::error('intent not found');
            return false;
        }
        return true;
    }

    public function validateSurveyResponse()
    {
        if (!isset($this->requestBody['response']) || !$this->requestBody['response']) {
            Log::error('response not found');
            return false;
        }
        return true;
    }

    function valid_RFC3339_EXTENDED_date($date)
    {
        if (\DateTime::createFromFormat(\DateTime::RFC3339_EXTENDED, $date) === FALSE) {
            Log::error('invalid datetime');
            return false;
        } else {
            return true;
        }
    }

}
