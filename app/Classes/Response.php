<?php


namespace App\Classes;


class Response
{
    private $response = [];
    private $code = 200;

    public function get_json(){
        return json_encode($this->response);
    }

    public function response($message = null, $code = null, array $errors = []) {

        $this->response = [
            'message' => $message,
            'errors' => $errors
        ];
        if ($code) {
            $this->code = $code;
        }
        return $this;
    }

    public function sendJSON() {
        response()->json($this->response, $this->code)->send();
        exit();
    }
}
