<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
  public function getAccountInfo(String $unique_name)
  {
      return $this->whereId($unique_name)->first();
  }
}
