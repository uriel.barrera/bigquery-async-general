<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Bots extends Model
{
//    protected $fillable = ['created_by', 'project_id', 'token', 'name', 'price_per_end_intent'];
    protected $table = 'bots';

    public static function getBotById(int $id)
    {
        return self::whereId($id)->get()->first();
    }

    public function getChatbotByIdAndToken(int $id, string $token)
    {
        return $this->whereId($id)->whereToken($token)->get()->first();
    }

    public function getChatbots($user_id)
    {
        return $this->whereUserId($user_id)->get();
    }

    public function getChatbotByProjectId($project_id)
    {
        return $this->whereProjectId($project_id)->first();
    }

    public function getAccount()
    {
        return $this->hasOne(Accounts::class, 'id', 'account_id')->first();
    }

    public function owner()
    {
        return $this->hasOne(Accounts::class, 'id', 'account_id')->first();
    }

    public function existColumn($column)
    {
        $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
        if (in_array($column, $columns)) {
            return true;
        } else {
            return false;
        }
    }

    public function nameExist(String $name)
    {
    }

    public function interface()
    {
//        DB::enableQueryLog(); // Enable query log
        return $this->hasOne(InterfaceChat::class, 'bot_id', 'id')->first();
//        dd(DB::getQueryLog()); // Show results of log
    }
}
