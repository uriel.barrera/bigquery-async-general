<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Color;

class BulkDownloadExport implements FromArray, WithHeadings, WithColumnFormatting, WithEvents
{
    use Exportable, RegistersEventListeners;

    protected $data;
    private $heads;

    public function __construct(array $data, $heads)
    {
        $this->data = $data;
        $this->heads = $heads;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return $this->heads;
    }

    public function columnFormats(): array
    {
        return [
//            'A' =>
//            'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function (BeforeExport $event) {
                $event->writer->getProperties()->setCreator('Mediaagility Mexico');

            },
            AfterSheet::class => function (AfterSheet $event) {
                $color_header = new Color();
                $color_header->setRGB(' #2471a3 ');
                $column = chr(64 + count($this->heads));
                $cellRange = 'A1:' . $column . '1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12)->setBold(true)->setColor($color_header);

                $styleArray = [
                    'font' => [
                        'bold' => true,
                        'color' => [
                            'argb' => 'FFFFFFFA'
                        ]
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'color' => [
                            'argb' => 'F8F9FAF7'
                        ]

                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => '2371B5FC',
                        ]
                    ],
                ];

                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);

                for ($x = 1; $x <= count($this->heads); $x++) {
                    $event->sheet->getDelegate()->getColumnDimension(chr(64 + $x))->setWidth(20);
                }
            },

            // Array callable, refering to a static method.
//            BeforeWriting::class => [self::class, 'beforeWriting'],

            // Using a class with an __invoke method.
//            BeforeSheet::class => new BeforeSheetHandler()
        ];
    }

    public static function beforeWriting(BeforeWriting $event)
    {
        //
    }

}
