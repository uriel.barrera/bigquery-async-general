<?php

if (!function_exists('credentials_path')) {
    function credentials_path($path = '')
    {
        return app()->make('path.credentials').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('credential')) {
    function credential($credential) {
        return base_path('credentials/'.config('app.project_id').'-'.$credential.'.json');
    }
}


