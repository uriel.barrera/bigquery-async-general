<?php
return [
    'dataset' => env('BIGQUERY_DATASET_LOGS', 'logs'),
    'table_chatbots' => env('BIGQUERY_TABLE_CHATBOT_LOGS', 'chatbots'),
    'table_monitor' => env('BIGQUERY_TABLE_MONITOR_LOGS', 'monitor'),
    'table_console' => env('BIGQUERY_TABLE_CONSOLE_LOGS', 'console')
];
