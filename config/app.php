<?php
return [
    'key' => env('APP_KEY'),
    'cipher' => 'AES-256-CBC',
    'project_id' => env('PROJECT_ID', 'mamx-chatbot-dev'),
    'folder_assets' => env('FOLDER_ASSETS', 'users'),
    'location_id' => env('LOCATION_ID', 'us-central1'),
];
