Servicio para insertar en bigquery

Este genera un query de insert con los datos que se le proporcionen y lo ejecuta, estos se insertan en bigquery, en la base de datos surveys, en la tabla de generic_survey, por ahora está configurado para el proyecto de mamx-chatbot-dev 

El servicio recibe por medio de POST los valores de: 


chatbot_id, como entero, obligatorio
session_id, obligatorio, cadena de texto
lang, cadena de texto con el lenguaje, es|en|fr
intent, cadena de texto
response, cadena de texto que se guarda con formato JSON
survey_response, cadena de texto que se guarda con formato JSON
tags, cadena de texto que se guarda con formato JSON
timestamp, cadena de texto en formato RFC3339_EXTENDED	
