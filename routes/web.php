<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'BigQuery Async v1 LTS';
});
$router->post('/', function () use ($router) {
    return 'BigQuery Async v1 LTS';
});

$router->get('/_ah/warmup', function () use ($router) {
    return 'Warmup successful';
});
$router->post('/_ah/warmup', function () use ($router) {
    return 'Warmup successful';
});

$router->group(['namespace' => 'V1', 'prefix' => 'v1'], function () use ($router) {
    $router->post('insertGenericSurvey', 'BigqueryController@insertGenericSurvey');
//    $router->get('chatbot_log', 'ReportsController@bulkDownload');
});
